#!/bin/python3

import math
import os
import itertools
import random
import re
import sys

# Complete the minimumDistances function below.
def minimumDistances(a):
    mdist = -1

    comb = list(itertools.combinations(a, 2))

    equal = set()
    for i in range(len(comb)):
        li = list(comb[i])
        if li[0] == li[1]:
            equal.add(li[0])

    equals = list(equal)
    if len(equals) == 0:
        return -1

    for i in range(len(equals)):
        
        index_list = list(filter(lambda x: a[x] == equals[i], range(len(a))))

        if mdist == -1:
            mdist = index_list[1] - index_list[0]


        if mdist > index_list[1] - index_list[0]:
            mdist = index_list[1] - index_list[0]

    return mdist

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    a = list(map(int, input().rstrip().split()))

    result = minimumDistances(a)

    fptr.write(str(result) + '\n')

    fptr.close()
 
